<footer>
  <img src="../../img/footer.png" style="max-width: 100%;height: auto;" class="bgfooter">
  <img src="../../img/footer_m.png" style="max-width: 100%;height: auto;" class="bgfooter_m">
  <div class="footer-area">
    <div class="container">
      <div class="row justify-content-between">
        <div class="col-xl-6 col-sm-6 col-lg-6">
          <div class="single-footer-widget footer_2">
            <h4>VACATION HOLIDAY TOURS</h4>
            <div class="contact_info">
              <span class="ti-home" style="color:#fff;font-size:20px;"></span>
              <p class="textFoot">322/18 ถนนรัชดาภิเษก ซอย 3 แยก 1</p>
              <p class="textFoot">แขวงดินแดง เขตดินแดง กรุงเทพฯ 10400</p>
            </div>
            <div class="contact_info">
              <p class="textFoot">ใบอนุญาตท่องเที่ยวเลขที่ 11/08796</p>
            </div>
            <div class="contact_info" style="margin-bottom: 0px;">
              <span class="ti-mobile" style="color:#fff;font-size:20px;top: 5px;"></span>
              <p class="textFoot"><a style="color:#fff" href="tel:0876883331" target="_blank" rel="nofollow">โทร : 087-688-3331 Soda , 064-956-9291 Moo</a></p>
            </div>
            <div class="contact_info" style="margin-bottom: 0px;">
              <span class="ti-email" style="color:#fff;font-size:20px;top: 5px;"></span>
              <p class="textFoot">Email : am_soda@gmail.com</p>
            </div>
            <div class="contact_info" style="margin-bottom: 0px;">
              <span class="ti-facebook" style="color:#fff;font-size:20px;top: 5px;"></span>
              <p class="textFoot"><a style="color:#fff" href="https://www.facebook.com/iget.visa.9" target="_blank" rel="nofollow">Facebook : Iget Visa</a></p>
            </div>
            <div class="contact_info" style="margin-bottom: 0px;">
              <span style="top: 0px;"><img src="img/line.png" style="height:20px;"></span>
              <p class="textFoot"><a style="color:#fff" href="https://line.me/R/ti/p/%40703xmmur" target="_blank" rel="nofollow">Line : SO087688</a></p>
            </div>
            <div class="contact_info" style="margin-bottom: 0px;">
              <span style="top: 0px;"><img src="img/wechat.png" style="height:20px;"></span>
              <p class="textFoot"><a style="color:#fff" href="" target="_blank" rel="nofollow">Wechat ID: So087688</a></p>
            </div>

          </div>
        </div>

        <div class="col-xl-6 col-sm-8 col-lg-6">
          <div class="single-footer-widget footer_3">
            <h4>เวลาทำการ</h4>
            <p class="textFoot">วันจันทร์ – ศุกร์ 09.00 am – 17.00pm</p>
            <p class="textFoot">นอกเหนือเวลาโปรด Add Line </p>
          </div>
        </div>
      </div>
    </div>
    <div class="copyright_part">
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-12 text-center">
            <p class="footer-text m-0">
              Copyright <span class="copyright">&copy;</span>
              <script>
                document.write(new Date().getFullYear());
              </script> All rights reserved by IGET VISA</p>
          </div>
        </div>
      </div>
    </div>
  </div>
</footer>

<script type="text/javascript">
  (function() {
    var options = {
      line: "//lin.ee/HnRsQL", // Line QR code URL
      call: "0876883331", // Call phone number
      call_to_action: "Message us", // Call to action
      button_color: "#FF6550", // Color of button
      position: "right", // Position may be 'right' or 'left'
      order: "line,call", // Order of buttons
    };
    var proto = document.location.protocol,
      host = "getbutton.io",
      url = proto + "//static." + host;
    var s = document.createElement('script');
    s.type = 'text/javascript';
    s.async = true;
    s.src = url + '/widget-send-button/js/init.js';
    s.onload = function() {
      WhWidgetSendButton.init(host, proto, options);
    };
    var x = document.getElementsByTagName('script')[0];
    x.parentNode.insertBefore(s, x);
  })();
</script>
<!-- /GetButton.io widget -->
<!-- footer part end-->

<!-- jquery plugins here-->

<script src="../../js/jquery-1.12.1.min.js"></script>
<!-- popper js -->
<script src="../../js/popper.min.js"></script>
<!-- bootstrap js -->
<script src="../../js/bootstrap.min.js"></script>
<!-- easing js -->
<script src="../../js/jquery.magnific-popup.js"></script>
<!-- swiper js -->
<script src="../../js/swiper.min.js"></script>
<!-- swiper js -->
<script src="../../js/masonry.pkgd.js"></script>
<!-- particles js -->
<script src="../../js/owl.carousel.min.js"></script>
<!-- swiper js -->
<script src="../../js/slick.min.js"></script>
<script src="../../js/gijgo.min.js"></script>
<script src="../../js/jquery.nice-select.min.js"></script>
<!-- contact js -->
<script src="../../js/jquery.ajaxchimp.min.js"></script>
<script src="../../js/jquery.form.js"></script>
<script src="../../js/jquery.validate.min.js"></script>
<script src="../../js/mail-script.js"></script>
<script src="../../js/contact.js"></script>
<!-- custom js -->
<script src="js/custom.js"></script>
