<header class="main_menu home_menu">
  <div class="container">
    <div class="row align-items-center">
      <div class="col-lg-12">
        <nav class="navbar navbar-expand-lg navbar-light">
          <a class="navbar-brand" href="../home/"><h2 style="color:#bf0d25">IGet Visa</h2></a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
              <span class="menu_icon"></span>
          </button>
          <div class="collapse navbar-collapse main-menu-item" id="navbarSupportedContent">
            <ul class="navbar-nav">
              <li class="nav-item">
                <a class="nav-link" href="../home/">หน้าแรก</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="../home/">เกี่ยวกับเรา</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="../home/">บริการ</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="../home/">ติดต่อเรา</a>
              </li>
            </ul>
          </div>
        </nav>
      </div>
    </div>
  </div>
</header>
