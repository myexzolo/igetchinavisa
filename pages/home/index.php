<!doctype html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>IGET VISA SERVICES</title>
  <?php include("../../inc/header.php"); ?>
  <style>
    th {
      text-align: center;
      background-color: #25c9e2;
      padding: 10px;
    }

    td {
      padding: 10px;
      background-color: #fafafa;
    }

    td.odd {
      background-color: #f5f5f5;
      padding: 10px;
    }

    li {
      padding-bottom: 5px;
    }

    table {
      clear: both;
      margin-top: 6px !important;
      margin-bottom: 6px !important;
      max-width: none !important;
      border-collapse: separate !important;
      /* border: 1px solid #f4f4f4; */
      border-spacing: 1;
      border-collapse: collapse;
      display: table;
      width: 100%;
      max-width: 100%;
    }
  </style>
</head>

<body>
  <!--::header part start::-->
<?php include("../../inc/menu.php"); ?>
  <!-- Header part end-->

  <!-- banner part start-->
  <section class="banner_part">
    <div class="container">
      <div class="row align-items-center">
        <div class="col-lg-6">
          <div class="banner_text">
            <div class="banner_text_iner">
              <h5>IGet Visa Services</h5>
              <h1 style="color:#333333">บริการรับทำวีซ่าจีน</h1>
              <p>วีซ่าท่องเที่ยว วีซ่าทำงาน วีซ่าแต่งงาน ระหว่างคนไทยและคนจีน และรวมไปถึงการรับรองเอกสาร ที่จะนำไปใช้ในประเทศจีน และรับปรึกษา วีซ่าทำงาน ประเทศ จีน ฮ่องกง โดยทีมงานมืออาชีพ มีประสบการณ์กว่า 14 ปี ให้ข้อมูลที่ถูกต้อง จัดการเอกสาร แนะนำ จริงใจ
                บริการรวดเร็ว ครบถ้วน ทุกรายละเอียด</p>
              <div class="banner_btn">
                <a href="https://line.me/R/ti/p/%40703xmmur" target="_blank" class="btn_1">สอบถามเรา</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section class="about_part">
    <div class="container-fluid">
      <div class="row align-items-center">
        <div class="col-md-4 col-lg-6">
          <div class="about_img ">
            <img src="../../img/stamp.png" class="about_img_1" alt="">
            <img src="../../img/pass.png" class="about_img_2" alt="">
            <img src="../../img/about_us_3.png" class="about_img_3" alt="">
          </div>
        </div>
        <div class="col-md-7 offset-md-1 col-lg-3 offset-lg-1">
          <div class="about_text">
            <h2>IGET VISA SERVICES </h2>
            <p style="font-size:16px;">บริการรับยื่นวีซ่าจีน เรามีประสบการณ์จริงและมีแนวทางที่ถูกต้อง เราพร้อมให้คำแนะนำด้านเอกสารวีซ่าสำหรับทุกท่าน การจัดเตรียมเอกสาร การกรอกแบบฟอร์มเอกสารทางการเงิน กลัวถูกปฏิเสธวีซ่าเราเข้าใจปัญหาของคุณลูกค้า เราพร้อมที่ช่วยวิเคราะห์และทำเรื่องยากให้เป็นเรื่องง่ายสำหรับคุณค่าใช้จ่าย
              เป็นราคารวมค่าวีซ่าจีน (ไม่รวมค่ารับส่ง)
            </p>
            <h4>ค่าธรรมเนียมการขอวีซ่าจีน</h4>
            <table clss="tableClass">
              <thead>
                <tr>
                  <th>ประเภทวิซ่า</th>
                  <th>รายละเอียด</th>
                </tr>
              </thead>
              <tdoby>
                <tr>
                  <td class="odd">
                    <div>ท่องเที่ยว L (30 วัน)</div>
                    <div>THAI / ไทย</div>
                  </td>
                  <td class="odd">
                    <div> เข้า - ออก : 1 ครั้ง </div>
                    <div> ทั่วไป (4 วันทำการ) : 2,000 ฿</div>
                    <div> ด่วน (2 วันทำการ) : 3,300 ฿</div>
                  </td>
                </tr>
                <tr>
                  <td>
                    <div>ท่องเที่ยว L (30 วัน)</div>
                    <div>THAI / ไทย</div>
                  </td>
                  <td>
                    <div> เข้า - ออก : 2 ครั้ง </div>
                    <div> ทั่วไป (4 วันทำการ) : 3,100 ฿</div>
                    <div> ด่วน (2 วันทำการ) : 4,400 ฿</div>
                  </td>
                </tr>
                <tr>
                  <td class="odd">
                    <div>ท่องเที่ยว X1 , X2</div>
                    <div>THAI / ไทย</div>
                  </td>
                  <td class="odd">
                    <div> เข้า - ออก : 1 ครั้ง </div>
                    <div> ทั่วไป (4 วันทำการ) : 2,700 ฿</div>
                    <div> ด่วน (2 วันทำการ) : 4,000 ฿</div>
                  </td>
                </tr>
                <tr>
                  <td>
                    <div>ธุรกิจ M</div>
                    <div>THAI / ไทย</div>
                  </td>
                  <td>
                    <div> เข้า - ออก : 1 ครั้ง </div>
                    <div> ทั่วไป (4 วันทำการ) : 2,700 ฿</div>
                    <div> ด่วน (2 วันทำการ) : 4,000 ฿</div>
                  </td>
                </tr>
                <tr>
                  <td class="odd">
                    <div>ทำงาน Z</div>
                    <div>THAI / ไทย</div>
                  </td>
                  <td class="odd">
                    <div> เข้า - ออก : 1 ครั้ง </div>
                    <div> ทั่วไป (4 วันทำการ) : 2,700 ฿</div>
                    <div> ด่วน (2 วันทำการ) : 4,000 ฿</div>
                  </td>
                </tr>
                <tr>
                  <td>
                    <div>เยี่ยมญาติ Q1 , Q2</div>
                    <div>THAI / ไทย</div>
                  </td>
                  <td>
                    <div> เข้า - ออก : 1 ครั้ง </div>
                    <div> ทั่วไป (4 วันทำการ) : 2,700 ฿</div>
                    <div> ด่วน (2 วันทำการ) : 4,000 ฿</div>
                  </td>
                </tr>
                <tr>
                  <td class="odd">
                    <div>Multiple Entries</div>
                    <div>THAI / ไทย</div>
                  </td>
                  <td class="odd">
                    <div> เข้า - ออก : ไม่จำกัดครั้ง</div>
                    <div> ติดต่อเจ้าหน้าที่</div>
                  </td>
                </tr>
                <tr>
                  <td>
                    <div>ท่องเที่ยว/ทำงาน/อื่นๆ</div>
                    <div>OTHER / ต่างชาติ</div>
                  </td>
                  <td>
                    <div> เข้า - ออก : - </div>
                    <div> ติดต่อเจ้าหน้าที่</div>
                  </td>
                </tr>
              </tdoby>
            </table>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section class="service_part section_padding2">
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-lg-7 col-lg-6 col-sm-10">
          <div class="section_tittle2">
            <!-- <img src="img/section_tittle_icon.png" alt="icon"> -->
            <h2>เอกสารที่ใช้ในการยื่นวีซ่าจีน</h2>
            </div>
        </div>
      </div>
      <div class="row">
      <div class="entry-content col-md-7">
        <ol>
          <li style="font-size: 16px;">หนังสือเดินทาง (passport) ตัวจริงที่มีอายุเหลือมากกว่า 6 เดือน และมีหน้าว่าง อีก 2 หน้าเป็นอย่างน้อย</li>
          <li style="font-size: 16px;">เอกสารยืนยันการจองตั๋วเครื่องบิน หรือ ใบจองตัวเครื่องบิน</li>
          <li style="font-size: 16px;">ใบจองโรงแรม</li>
          <li style="font-size: 16px;">สำเนาบัตรประชาชน เซ็นต์รับรองสำเนาถูกต้อง</li>
          <li style="font-size: 16px;">รูปถ่าย ขนาด 33 มม. x 48 มม. จำนวน 2 ใบ อายุไม่เกิน 6 เดือน โดยในภาพถ่ายจะต้อง<br>
            – มีฉากหลังขาว<br>
            – ไม่ใส่เสื้อแขนกุด<br>
            – ไม่ใส่ชุดครุย หรือชุดข้าราชการ<br>
            – ห้ามยิ้ม<br>
            – ต้องเห็นหู<br>
            – พื้นหลังสีขาวเท่านั้น<br>
            – ห้ามใส่เสื้อสีขาว<br>
            – ถ่ายหน้าตรง<br>
            – ห้ามใส่แว่นตาสีดำ<br>
            – คนมุสลิมเปิดเห็นใบหน้า<br>
            – ห้ามใส่หมวก<br>
            – ห้ามสวมใส่เครื่องประดับ<br>
          </li>
        </ol>
        <div style="color:#ccc;font-size:16px;margin-bottom:5px;"><b>สำหรับ เด็กอายุต่ำกว่า 18 ปี เพิ่มเติมเอกสารดังนี้</b></div>
        <ol>
          <li style="font-size: 16px;">สำเนาสูติบัตรรับทำวีซ่าจีน</li>
          <li style="font-size: 16px;">ชื่อโรงเรียน หรือ สถานศึกษา</li>
          <li style="font-size: 16px;">สำเนาใบเปลี่ยนชื่อ-สกุล (กรณีไม่ตรงกับสูติบัตร) </li>
          <li style="font-size: 16px;">กรณีเดินทางกับบิดา หรือ มารดา ท่านใดท่านหนึ่ง<br>
            – เพิ่มสำเนาบัตรประชาชนของท่านที่ไม่ได้เดินทาง <br>
            – เพิ่มเติมจดหมายเขียนรับรองว่า เด็กเดินทางไปกับใคร <br>
          </li>
          <li style="font-size: 16px;">กรณีไม่ได้เดินทางกับบิดาและมารดา<br>
            – ต้องมีหนังสือยินยอมให้เดินทางไปต่างประเทศ <span style="color:red">*** ขอได้ที่สำนักงานเขตที่แจ้งเกิด***<span>
        </ol>
      </div>
      <div class="col-md-5">
        <div align="center"><p>ตัวอย่างรูปถ่ายที่ถูกต้อง<p/>
        <div class="single_service_part" >
          <img src="../../img/photoForVisa.png" alt="" style="margin-bottom:30px;">
        </div>
      </div>
      </div>
    </div>
    <br>
    <p>ทางบริษัท ดำเนินกิจการถูกต้องตามกฎหมายไม่สนับสนุน การขอวีซ่าเพื่อแอบเข้าไปทำงาน “ผิดกฎหมาย” ใดๆทั้งสิ้นและไม่รับปลอมเอกสารหรือข้อมูลทุกกรณี(เรายื่นด้วยข้อมูลจริงเท่านั้น เพื่อประโยชน์ของตัวลูกค้าเอง)</p>
  </div>
  </section>

  <!-- footer part start-->
<?php include("../../inc/footer.php"); ?>
</body>

</html>
